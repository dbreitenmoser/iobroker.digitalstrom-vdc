import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { CreateCSSProperties } from "@material-ui/core/styles/withStyles";
import TextField from "@material-ui/core/TextField";
import Input from "@material-ui/core/Input";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import MaUTable from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import IconAdd from "@material-ui/icons/Add";
import IconSearch from "@material-ui/icons/SearchOutlined";
import IconAdd2 from "@material-ui/icons/AddCircleOutline";
import {
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    Snackbar,
    Tooltip,
    IconButton,
    Link,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import Box from "@material-ui/core/Box";
import { Info } from "@material-ui/icons";
// import { Typography } from "@material-ui/core";
import I18n from "@iobroker/adapter-react/i18n";
import Connection from "@iobroker/adapter-react/Connection";

import "regenerator-runtime/runtime";

// import DialogSelectID from "@iobroker/adapter-react/Dialogs/SelectID";

const styles = (): Record<string, CreateCSSProperties> => ({
    input: {
        marginTop: 0,
        minWidth: 400,
    },
    button: {
        marginRight: 20,
    },
    card: {
        maxWidth: 345,
        textAlign: "center",
    },
    media: {
        height: 180,
    },
    column: {
        display: "inline-block",
        verticalAlign: "top",
        marginRight: 20,
    },
    columnLogo: {
        width: 350,
        marginRight: 0,
    },
    columnSettings: {
        width: "calc(100% - 370px)",
    },
    controlElement: {
        //background: "#d2d2d2",
        marginBottom: 5,
    },
});

const colorClassOptions: { value: string; title: AdminWord }[] = [
    {
        value: "1",
        title: "yellowColorClass",
    },
    {
        value: "2",
        title: "greyColorClass",
    },
    {
        value: "3",
        title: "blueColorClass",
    },
    {
        value: "4",
        title: "cyanColorClass",
    },
    {
        value: "5",
        title: "magentaColorClass",
    },
    {
        value: "6",
        title: "redColorClass",
    },
    {
        value: "7",
        title: "greenColorClass",
    },
    {
        value: "8",
        title: "blackColorClass",
    },
    {
        value: "9",
        title: "whiteColorClass",
    },
];

/*const groupOptions: { value: string; title: AdminWord }[] = [
    {
        value: "1",
        title: "yellowColorClass",
    },
    {
        value: "2",
        title: "greyColorClass",
    },
    {
        value: "3",
        title: "blueGroupHeating",
    },
    {
        value: "4",
        title: "cyanColorClass",
    },
    {
        value: "5",
        title: "magentaColorClass",
    },
    {
        value: "6",
        title: "redColorClass",
    },
    {
        value: "7",
        title: "greenColorClass",
    },
    {
        value: "8",
        title: "blackGroup",
    },
    {
        value: "9",
        title: "blueGroupCooling",
    },
    {
        value: "10",
        title: "blueGroupVentilation",
    },
    {
        value: "11",
        title: "blueGroupWindows",
    },
    {
        value: "12",
        title: "blueGroupAirrecirculation",
    },
    {
        value: "48",
        title: "groupRoomTemperatureControl",
    },
    {
        value: "49",
        title: "groupRoomVentilationControl",
    },
];*/
interface unitValue {
    symbol: string;
    unit: string;
}

const units: { value: unitValue; title: string }[] = [
    {
        value: { symbol: "lx", unit: "lux" },
        title: "lux",
    },
    {
        value: { symbol: "°C", unit: "celsius" },
        title: "celsius",
    },
    {
        value: { symbol: "%", unit: "percent" },
        title: "percent",
    },
    {
        value: { symbol: "m/s", unit: "m/s" },
        title: "m/s",
    },
    {
        value: { symbol: "W", unit: "watt" },
        title: "watt",
    },
    {
        value: { symbol: "V", unit: "volt" },
        title: "volt",
    },
    {
        value: { symbol: "kWh", unit: "kWh" },
        title: "kWh",
    },
    {
        value: { symbol: "A", unit: "ampere" },
        title: "ampere",
    },
    {
        value: { symbol: "μg/m3", unit: "μg/m3" },
        title: "μg/m3",
    },
    {
        value: { symbol: "ppm", unit: "ppm" },
        title: "ppm",
    },
    {
        value: { symbol: "mm/m2", unit: "mm/m2" },
        title: "mm/m2",
    },
];

const outputLightOptions: { value: string; title: AdminWord }[] = [
    {
        value: "basic",
        title: "outputLightBasic",
    },
    {
        value: "light",
        title: "outputLightLight",
    },
    {
        value: "colorlight",
        title: "outputLightColorlight",
    },
    {
        value: "ctlight",
        title: "outputLightCTLight",
    },
];

const outputShadowOptions: { value: string; title: AdminWord }[] = [
    {
        value: "roller",
        title: "outputShadowRoller",
    },
    {
        value: "sun",
        title: "outputShadowSun",
    },
    {
        value: "jalousie",
        title: "outputShadowJalousie",
    },
];

const outputVentilationOptions: { value: string; title: AdminWord }[] = [
    {
        value: "ventilation",
        title: "outputVentilationVentilation",
    },
    {
        value: "recirculation",
        title: "outputVentilationRecirculation",
    },
];

const deviceTypeOptions: { value: string; title: AdminWord }[] = [
    {
        value: "lamp",
        title: "deviceTypeOptionsLamp",
    },
    {
        value: "rgbLamp",
        title: "deviceTypeOptionsRGBLamp",
    },
    {
        value: "sensor",
        title: "deviceTypeOptionsSensor",
    },
    {
        value: "presenceSensor",
        title: "deviceTypeOptionsMotionDetection",
    },
    {
        value: "smokeAlarm",
        title: "deviceTypeOptionsSmokeAlarm",
    },
    {
        value: "button",
        title: "deviceTypeOptionsButton",
    },
    {
        value: "doorbell",
        title: "deviceTypeOptionsDoorbell",
    },
    {
        value: "awayButton",
        title: "deviceTypeOptionsAwayButton",
    },
    {
        value: "multiSensor",
        title: "deviceTypeOptionsMultiSensor",
    },
];

const sensorTypeOptions: { value: string; title: AdminWord }[] = [
    {
        value: "0",
        title: "sensorTypeNone",
    },
    {
        value: "1",
        title: "sensorTypeTemperature",
    },
    {
        value: "2",
        title: "sensorTypeHumidity",
    },
    {
        value: "3",
        title: "sensorTypeIllumination",
    },
    {
        value: "4",
        title: "sensorTypeSupplyVoltage",
    },
    {
        value: "5",
        title: "sensorTypeCO2",
    },
    {
        value: "6",
        title: "sensorTypeRadon",
    },
    {
        value: "7",
        title: "sensorTypeGas",
    },
    {
        value: "8",
        title: "sensorTypePM10",
    },
    {
        value: "9",
        title: "sensorTypePM25",
    },
    {
        value: "10",
        title: "sensorTypePM01",
    },
    {
        value: "11",
        title: "sensorTypeRoomOperatingPanel",
    },
    {
        value: "12",
        title: "sensorTypeFanSpeed",
    },
    {
        value: "13",
        title: "sensorTypeWindSpeed",
    },
    {
        value: "14",
        title: "sensorTypeActivePower",
    },
    {
        value: "15",
        title: "sensorTypeElectricCurrent",
    },
    {
        value: "16",
        title: "sensorTypeEnergyMeter",
    },
    {
        value: "17",
        title: "sensorTypeApparentPower",
    },
    {
        value: "18",
        title: "sensorTypeAirPressure",
    },
    {
        value: "19",
        title: "sensorTypeWindDirection",
    },
    {
        value: "20",
        title: "sensorTypeSoundPressure",
    },
    {
        value: "21",
        title: "sensorTypePercipitation",
    },
    {
        value: "22",
        title: "sensorTypeCO2concentration",
    },
    {
        value: "23",
        title: "sensorTypeWindGustSpeed",
    },
    {
        value: "24",
        title: "sensorTypeWindGustDirection",
    },
    {
        value: "25",
        title: "sensorTypeGeneratedActivePower",
    },
    {
        value: "26",
        title: "sensorTypeGeneratedEnergy",
    },
    {
        value: "27",
        title: "sensorTypeWaterQuality",
    },
    {
        value: "28",
        title: "sensorTypeWaterFlowRate",
    },
];

const sensorUsageOptions: { value: string; title: AdminWord }[] = [
    {
        value: "0",
        title: "sensorUsageUndefined",
    },
    {
        value: "1",
        title: "sensorUsageRoom",
    },
    {
        value: "2",
        title: "sensorUsageOutdoor",
    },
];

/*const sensorFunctionOptions: { value: string; title: AdminWord }[] = [
    {
        value: "0",
        title: "sensorFunctionAppMode",
    },
    {
        value: "1",
        title: "sensorFunctionPresence",
    },
    {
        value: "4",
        title: "sensorFunctionTwilight",
    },
    {
        value: "5",
        title: "sensorFunctionMotionDetector",
    },
    {
        value: "7",
        title: "sensorFunctionSmokeDetector",
    },
    {
        value: "8",
        title: "sensorFunctionWindMonitor",
    },
    {
        value: "9",
        title: "sensorFunctionRainMonitor",
    },
    {
        value: "10",
        title: "sensorFunctionSunRadiation",
    },
    {
        value: "11",
        title: "sensorFunctionThermostat",
    },
    {
        value: "12",
        title: "sensorFunctionBatteryLow",
    },
    {
        value: "13",
        title: "sensorFunctionWindowContact",
    },
    {
        value: "14",
        title: "sensorFunctionDoorContact",
    },
    {
        value: "15",
        title: "sensorFunctionWindowHandle",
    },
    {
        value: "16",
        title: "sensorFunctionGarageDoor",
    },
    {
        value: "17",
        title: "sensorFunctionSunProtection",
    },
    {
        value: "18",
        title: "sensorFunctionFrostDetection",
    },
    {
        value: "19",
        title: "sensorFunctionHeatingEnabled",
    },
    {
        value: "20",
        title: "sensorFunctionHeatingChangeOver",
    },
    {
        value: "21",
        title: "sensorFunctionInitializationStatus",
    },
    {
        value: "22",
        title: "sensorFunctionMalfunction",
    },
    {
        value: "23",
        title: "sensorFunctionService",
    },
];*/

interface AddDevicesProps {
    classes: Record<string, string>;
    native: Record<string, any>;
    allObjects: Array<any>;
    socket: Connection;
    adapterName: string;
    instance: number;
    tabValue: number;
    sendToAddDevice: any;
    updateLastDeviceUpdate: any;
    onChange: (attr: string, value: any) => void;
}

interface AddDevicesState {
    // add your state properties here
    dummy?: undefined;
    channelFilter: string;
    toast: string;
    addDevice: any;
    addStates: Array<any>;
    addDeviceName: string;
    addDeviceConfigURL: string;
    addDeviceDeviceType: string;
    basicLampPowerSwitch: string;
    basicButton: string;
    addDeviceSensorColorClass: string;
    addDeviceSensorResolution: string;
    addDeviceSensorState: string;
    addDeviceSensorType: string;
    addDeviceSensorUsage: string;
    addDeviceSensorMax: string;
    addDeviceSensorMin: string;
    addDeviceSensorSIUnit: string;
    addDeviceSensorSymbol: string;
    confirmMsg: boolean;
    showCreateConfirmation: boolean;
    manualDeviceId: string;
    showSelectId: boolean;
    selectIdValue: string | string[] | undefined;
    basicDoorbell: string;
    addDeviceRGBLampPowerSwitch: string;
    addDeviceRGBLampColormode: string;
    addDeviceRGBLampDimmer: string;
    addDeviceRGBLampColortemp: string;
    addDeviceRGBLampHue: string;
    addDeviceRGBLampSaturation: string;
    addDeviceRGBLampRGB: string;
    sensorList: Array<any>;
    unitValue: unitValue;
    sensorMultiplier: string;
}

class AddDevices extends React.Component<AddDevicesProps, AddDevicesState> {
    static displayName = "Settings";

    constructor(props: AddDevicesProps) {
        super(props);
        this.state = {
            channelFilter: "",
            toast: "",
            addDevice: {},
            addStates: [],
            addDeviceName: "",
            addDeviceConfigURL: "",
            addDeviceDeviceType: "",
            basicLampPowerSwitch: "",
            addDeviceSensorState: "",
            addDeviceSensorColorClass: "",
            addDeviceSensorType: "",
            addDeviceSensorUsage: "",
            addDeviceSensorMax: "",
            addDeviceSensorMin: "",
            addDeviceSensorSIUnit: "",
            addDeviceSensorSymbol: "",
            confirmMsg: false,
            showCreateConfirmation: false,
            manualDeviceId: "",
            showSelectId: false,
            selectIdValue: "",
            basicButton: "",
            basicDoorbell: "",
            addDeviceSensorResolution: "1",
            addDeviceRGBLampPowerSwitch: "",
            addDeviceRGBLampColormode: "",
            addDeviceRGBLampDimmer: "",
            addDeviceRGBLampColortemp: "",
            addDeviceRGBLampHue: "",
            addDeviceRGBLampSaturation: "",
            addDeviceRGBLampRGB: "",
            sensorList: [],
            unitValue: { unit: "", symbol: "" },
            sensorMultiplier: "1",
        };
    }

    getAllChannels() {
        let channels: Array<any> = [];
        // console.log(this.props.allObjects);
        if (this.state.channelFilter && this.state.channelFilter.length > 2) {
            if (this.props.allObjects && this.props.allObjects.length > 0) {
                channels = this.props.allObjects.filter((o) => {
                    return (
                        (o.type === "channel" || o.type === "device" || o.type === "folder") &&
                        o.from &&
                        o.from.length > 0 &&
                        o.from.toLowerCase().includes(this.state.channelFilter.toLowerCase())
                    );
                });
            }
        }
        return channels;
    }

    getAllStatesOfChannel(deviceId: string) {
        let states: Array<any> = [];
        if (deviceId && deviceId.length > 0) {
            if (this.props.allObjects && this.props.allObjects.length > 0) {
                states = this.props.allObjects.filter((o) => {
                    return o.type === "state" && o._id.includes(deviceId);
                });
            }
        }
        return states;
    }

    addNewDevice(event: any, device: any) {
        this.setState({ channelFilter: "" });
        // console.log("ADD NEW DEVICE", device);
        const deviceStates = this.getAllStatesOfChannel(device._id);
        // console.log(deviceStates);
        this.setState({
            addDevice: device,
            addStates: deviceStates,
            toast: "New Device",
        });
    }

    renderInput(title: AdminWord, attr: string, type: string) {
        return (
            <TextField
                label={I18n.t(title)}
                className={`${this.props.classes.input} ${this.props.classes.controlElement}`}
                value={this.props.native[attr]}
                type={type || "text"}
                onChange={(e) =>
                    this.setState({ [attr]: e.target.value } as unknown as Pick<AddDevicesState, keyof AddDevicesState>)
                }
                margin="normal"
            />
        );
    }

    renderSelect(
        title: AdminWord,
        attr: string,
        options: { value: string; title: AdminWord }[],
        style?: React.CSSProperties,
    ) {
        return (
            <FormControl
                className={`${this.props.classes.input} ${this.props.classes.controlElement}`}
                style={{
                    paddingTop: 5,
                    ...style,
                }}
            >
                <Select
                    value={this.state[attr] || "_"}
                    onChange={(e) =>
                        this.setState({ [attr]: e.target.value === "_" ? "" : e.target.value } as unknown as Pick<
                            AddDevicesState,
                            keyof AddDevicesState
                        >)
                    }
                    input={<Input name={attr} id={attr + "-helper"} />}
                >
                    {options.map((item) => (
                        <MenuItem key={"key-" + item.value} value={item.value || "_"}>
                            {I18n.t(item.title)}
                        </MenuItem>
                    ))}
                </Select>
                <FormHelperText>{I18n.t(title)}</FormHelperText>
            </FormControl>
        );
    }

    renderSelectUnit(
        title: AdminWord,
        attr: string,
        options: { value: unitValue; title: string }[],
        style?: React.CSSProperties,
    ) {
        return (
            <FormControl
                className={`${this.props.classes.input} ${this.props.classes.controlElement}`}
                style={{
                    paddingTop: 5,
                    ...style,
                }}
            >
                <Select
                    value={this.state[attr] || "_"}
                    onChange={(e) => {
                        const value = units.find((u) => u.title == e.target.value);
                        this.setState({
                            [attr]: e.target.value === "_" ? "" : value,
                        } as unknown as Pick<AddDevicesState, keyof AddDevicesState>);
                    }}
                    input={<Input name={attr} id={attr + "-helper"} />}
                >
                    {options.map((item, i) => (
                        <MenuItem key={"key-" + i} value={item.title || "_"}>
                            {item.title}
                        </MenuItem>
                    ))}
                </Select>
                <FormHelperText>{I18n.t(title)}</FormHelperText>
            </FormControl>
        );
    }

    renderSelectString(
        title: AdminWord,
        attr: string,
        options: { value: string; title: string }[],
        style?: React.CSSProperties,
    ) {
        return (
            <FormControl
                className={`${this.props.classes.input} ${this.props.classes.controlElement}`}
                style={{
                    paddingTop: 5,
                    ...style,
                }}
            >
                <Select
                    value={this.state[attr] || "_"}
                    onChange={(e) =>
                        this.setState({ [attr]: e.target.value === "_" ? "" : e.target.value } as unknown as Pick<
                            AddDevicesState,
                            keyof AddDevicesState
                        >)
                    }
                    input={<Input name={attr} id={attr + "-helper"} />}
                >
                    {options.map((item) => (
                        <MenuItem key={"key-" + item.value} value={item.value || "_"}>
                            {item.title}
                        </MenuItem>
                    ))}
                </Select>
                <FormHelperText>{I18n.t(title)}</FormHelperText>
            </FormControl>
        );
    }

    renderCheckbox(title: AdminWord, attr: string, style?: React.CSSProperties) {
        return (
            <FormControlLabel
                key={attr}
                style={{
                    paddingTop: 5,
                    ...style,
                }}
                className={this.props.classes.controlElement}
                control={
                    <Checkbox
                        checked={this.props.native[attr]}
                        onChange={() => this.props.onChange(attr, !this.props.native[attr])}
                        color="primary"
                    />
                }
                label={I18n.t(title)}
            />
        );
    }

    renderChannelFilter() {
        return (
            <TextField
                label={I18n.t("channelFilter")}
                className={`${this.props.classes.input} ${this.props.classes.controlElement}`}
                value={this.state.channelFilter}
                type="text"
                onChange={(e) => this.setState({ channelFilter: e.target.value })}
                margin="normal"
            />
        );
    }

    renderAvailableChannels() {
        // Render the UI for your table
        return (
            <div className="container">
                <MaUTable>
                    <TableHead>
                        <TableRow>
                            <TableCell key="1">{I18n.t("objectsFrom")}</TableCell>
                            <TableCell key="2">{I18n.t("objectsName")}</TableCell>
                            <TableCell key="3">{I18n.t("objectsAction")}</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.getAllChannels().map((row, i) => (
                            <React.Fragment key={i}>
                                <TableRow>
                                    <TableCell>{row["from"]}</TableCell>
                                    <TableCell>{row["common"]["name"]}</TableCell>
                                    <TableCell>
                                        <Button
                                            onClick={(e) => {
                                                this.addNewDevice(e, row);
                                            }}
                                            variant="outlined"
                                        >
                                            <IconAdd /> {I18n.t("addButton")}
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            </React.Fragment>
                        ))}
                    </TableBody>
                </MaUTable>
            </div>
        );
    }

    renderLightOptions() {
        if (this.props.native["addDeviceColorClass"] == "1") {
            return (
                <Grid item xs={6}>
                    {this.renderSelect("output", "addDeviceOutput", outputLightOptions)}
                </Grid>
            );
        } else {
            return null;
        }
    }

    renderShadowOptions() {
        if (this.props.native["addDeviceColorClass"] == "2") {
            return (
                <Grid item xs={6}>
                    {this.renderSelect("shadowKind", "addDeviceKind", outputShadowOptions)}
                </Grid>
            );
        } else {
            return null;
        }
    }

    renderVentilationOptions() {
        if (this.props.native["addDeviceGroup"] == "10") {
            return (
                <Grid item xs={6}>
                    {this.renderSelect("ventilationKind", "addDeviceKind", outputVentilationOptions)}
                </Grid>
            );
        } else {
            return null;
        }
    }

    genDSUID() {
        const genRanHex = (size) => [...Array(size)].map(() => Math.floor(Math.random() * 16).toString(16)).join("");
        return genRanHex(34).toUpperCase();
    }

    renderBasicLamp() {
        if (this.state.addDeviceDeviceType != "lamp") return null;
        const options: { value: string; title: string }[] = [];
        this.state.addStates.map((state) => {
            // console.log(state);
            options.push({ value: state._id, title: `${state.common.name} - ${state.common.role}` });
        });
        return (
            <div id="basicLamp">{this.renderSelectString("selectPowerSwitch", "basicLampPowerSwitch", options)}</div>
        );
    }

    renderButton() {
        if (this.state.addDeviceDeviceType != "button") return null;
        const options: { value: string; title: string }[] = [];
        this.state.addStates.map((state) => {
            // console.log(state);
            options.push({ value: state._id, title: `${state.common.name} - ${state.common.role}` });
        });
        return <div id="button">{this.renderSelectString("selectButtonSwitch", "basicButton", options)}</div>;
    }

    renderAway() {
        if (this.state.addDeviceDeviceType != "awayButton") return null;
        const options: { value: string; title: string }[] = [];
        this.state.addStates.map((state) => {
            // console.log(state);
            options.push({ value: state._id, title: `${state.common.name} - ${state.common.role}` });
        });
        return <div id="button">{this.renderSelectString("selectButtonSwitch", "basicButton", options)}</div>;
    }

    renderDoorbell() {
        if (this.state.addDeviceDeviceType != "doorbell") return null;
        const options: { value: string; title: string }[] = [];
        this.state.addStates.map((state) => {
            // console.log(state);
            options.push({ value: state._id, title: `${state.common.name} - ${state.common.role}` });
        });
        return <div id="doorbell">{this.renderSelectString("selectDoorbellSwitch", "basicDoorbell", options)}</div>;
    }

    renderDimLamp() {
        return null;
    }

    findRGBLampValues() {
        console.log(this.state.addStates);
        this.state.addStates.forEach((state) => {
            console.log(state);
            if (state && state.common && state.common.role) {
                if (state.common.role == "level.dimmer") {
                    console.log(state._id);
                    this.setState({ addDeviceRGBLampDimmer: state._id });
                } else if (state.common.role == "switch") {
                    console.log(state._id);
                    this.setState({ addDeviceRGBLampPowerSwitch: state._id });
                } else if (state.common.role == "switch.mode.color") {
                    console.log(state._id);
                    this.setState({ addDeviceRGBLampColormode: state._id });
                } else if (state.common.role == "level.color.hue") {
                    console.log(state._id);
                    this.setState({ addDeviceRGBLampHue: state._id });
                } else if (state.common.role == "level.color.temperature") {
                    console.log(state._id);
                    this.setState({ addDeviceRGBLampColortemp: state._id });
                } else if (state.common.role == "level.rgb") {
                    console.log(state._id);
                    this.setState({ addDeviceRGBLampRGB: state._id });
                } else if (state.common.role == "level.color.saturation") {
                    console.log(state._id);
                    this.setState({ addDeviceRGBLampSaturation: state._id });
                }
            }
        });
    }

    renderRGBLamp() {
        console.log(this.state.addDeviceDeviceType);
        if (this.state.addDeviceDeviceType != "rgbLamp") return null;
        const options: { value: string; title: string }[] = [];
        this.state.addStates.map((state) => {
            // console.log(state);
            options.push({ value: state._id, title: `${state.common.name} - ${state.common.role}` });
        });
        return (
            <div id="rgblamp">
                <Grid container spacing={2}>
                    <Grid item xs={6}>
                        <Button
                            onClick={() => {
                                this.findRGBLampValues();
                            }}
                            variant="outlined"
                        >
                            <IconSearch /> {I18n.t("detectValues")}
                        </Button>
                    </Grid>
                </Grid>
                <Grid container spacing={2}>
                    <Grid item xs={6}>
                        {this.renderSelectString("selectPowerSwitch", "addDeviceRGBLampPowerSwitch", options)}
                    </Grid>
                    <Grid item xs={6}>
                        {this.renderSelectString("selectColormode", "addDeviceRGBLampColormode", options)}
                    </Grid>
                </Grid>
                <Grid container spacing={2}>
                    <Grid item xs={6}>
                        {this.renderSelectString("selectDimmer", "addDeviceRGBLampDimmer", options)}
                    </Grid>
                    <Grid item xs={6}>
                        {this.renderSelectString("selectColortemp", "addDeviceRGBLampColortemp", options)}
                    </Grid>
                </Grid>
                <Grid container spacing={2}>
                    <Grid item xs={6}>
                        {this.renderSelectString("selectHue", "addDeviceRGBLampHue", options)}
                    </Grid>
                    <Grid item xs={6}>
                        {this.renderSelectString("selectSaturation", "addDeviceRGBLampSaturation", options)}
                    </Grid>
                </Grid>
                <Grid container spacing={2}>
                    <Grid item xs={6}>
                        {this.renderSelectString("selectRGB", "addDeviceRGBLampRGB", options)}
                    </Grid>
                </Grid>
            </div>
        );
    }

    renderSensor() {
        if (this.state.addDeviceDeviceType != "sensor") return null;
        const options: { value: string; title: string }[] = [];
        this.state.addStates.map((state) => {
            console.log(state);
            options.push({ value: state._id, title: `${state.common.name} - ${state.common.role}` });
        });
        return (
            <div id="sensor">
                <Grid container spacing={2}>
                    <Grid item xs={6}>
                        {this.renderSelectString("selectSensor", "addDeviceSensorState", options)}
                    </Grid>
                    <Grid item xs={6}>
                        {this.renderSelect("colorClass", "addDeviceSensorColorClass", colorClassOptions)}
                    </Grid>
                </Grid>
                <Grid container spacing={2}>
                    <Grid item xs={6}>
                        {this.renderSelect("sensorType", "addDeviceSensorType", sensorTypeOptions)}
                    </Grid>
                    <Grid item xs={6}>
                        {this.renderSelect("sensorUsage", "addDeviceSensorUsage", sensorUsageOptions)}
                    </Grid>
                </Grid>
                <Grid container spacing={2}>
                    <Grid item xs={6}>
                        {this.renderInput("addDeviceSensorMax", "addDeviceSensorMax", "text")}
                    </Grid>
                    <Grid item xs={6}>
                        {this.renderInput("addDeviceSensorMin", "addDeviceSensorMin", "text")}
                    </Grid>
                </Grid>
                <Grid container spacing={2}>
                    <Grid item xs={6}>
                        {this.renderInput("addDeviceSensorResolution", "addDeviceSensorResolution", "text")}
                        <Tooltip title={I18n.t("tooltipResolution")}>
                            <IconButton>
                                <Info />
                            </IconButton>
                        </Tooltip>
                    </Grid>
                </Grid>

                <Grid container spacing={2}>
                    <Grid item xs={6}>
                        {this.renderSelectUnit("addDeviceSIUnit", "unitValue", units)}
                    </Grid>
                    <Grid item xs={6}>
                        {this.renderInput("sensorMutation", "sensorMultiplier", "text")}
                        <Tooltip title={I18n.t("sensorMutationTooltip")}>
                            <IconButton>
                                <Info />
                            </IconButton>
                        </Tooltip>
                    </Grid>
                </Grid>
            </div>
        );
    }

    renderMultiSensor() {
        if (this.state.addDeviceDeviceType != "multiSensor") return null;
        const options: { value: string; title: string }[] = [];
        this.state.addStates.map((state) => {
            console.log(state);
            options.push({ value: state._id, title: `${state.common.name} - ${state.common.role}` });
        });
        return (
            <div id="sensor">
                <div id="sensorList">
                    <Grid container spacing={2}>
                        <Grid item xs={4}>
                            {I18n.t("sensorType")}
                        </Grid>
                        <Grid item xs={4}>
                            {I18n.t("colorClass")}
                        </Grid>
                        <Grid item xs={4}>
                            {I18n.t("sensorUsage")}
                        </Grid>
                    </Grid>
                    <hr />
                    {this.state.sensorList.map((s, i) => (
                        <Grid container spacing={2} key={i}>
                            <Grid item xs={4}>
                                {s.sensorType}
                            </Grid>
                            <Grid item xs={4}>
                                {s.colorClass}
                            </Grid>
                            <Grid item xs={4}>
                                {s.sensorUsage}
                            </Grid>
                        </Grid>
                    ))}
                </div>
                <div id="addSensor">
                    <Grid container spacing={2}>
                        <Grid item xs={6}>
                            {this.renderSelectString("selectSensor", "addDeviceSensorState", options)}
                        </Grid>
                        <Grid item xs={6}>
                            {this.renderSelect("colorClass", "addDeviceSensorColorClass", colorClassOptions)}
                        </Grid>
                    </Grid>
                    <Grid container spacing={2}>
                        <Grid item xs={6}>
                            {this.renderSelect("sensorType", "addDeviceSensorType", sensorTypeOptions)}
                        </Grid>
                        <Grid item xs={6}>
                            {this.renderSelect("sensorUsage", "addDeviceSensorUsage", sensorUsageOptions)}
                        </Grid>
                    </Grid>
                    <Grid container spacing={2}>
                        <Grid item xs={6}>
                            {this.renderInput("addDeviceSensorMax", "addDeviceSensorMax", "text")}
                        </Grid>
                        <Grid item xs={6}>
                            {this.renderInput("addDeviceSensorMin", "addDeviceSensorMin", "text")}
                        </Grid>
                    </Grid>
                    <Grid container spacing={2}>
                        <Grid item xs={6}>
                            {this.renderInput("addDeviceSensorResolution", "addDeviceSensorResolution", "text")}
                            <Tooltip title={I18n.t("tooltipResolution")}>
                                <IconButton>
                                    <Info />
                                </IconButton>
                            </Tooltip>
                        </Grid>
                    </Grid>

                    <Grid container spacing={2}>
                        <Grid item xs={6}>
                            {this.renderSelectUnit("addDeviceSIUnit", "unitValue", units)}
                        </Grid>
                        <Grid item xs={6}>
                            {this.renderInput("sensorMutation", "sensorMultiplier", "text")}
                            <Tooltip title={I18n.t("sensorMutationTooltip")}>
                                <IconButton>
                                    <Info />
                                </IconButton>
                            </Tooltip>
                        </Grid>
                    </Grid>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <Button
                                onClick={() => {
                                    const sensorList = this.state.sensorList;
                                    sensorList.push({
                                        selectSensor: this.state.addDeviceSensorState,
                                        colorClass: this.state.addDeviceSensorColorClass,
                                        sensorType: this.state.addDeviceSensorType,
                                        sensorUsage: this.state.addDeviceSensorUsage,
                                        addDeviceSensorMax: this.state.addDeviceSensorMax,
                                        addDeviceSensorMin: this.state.addDeviceSensorMin,
                                        addDeviceSensorResolution: this.state.addDeviceSensorResolution,
                                        addDeviceSensorSymbol: this.state.unitValue.symbol,
                                        addDeviceSensorSIUnit: this.state.unitValue.unit,
                                        modifier: this.state.sensorMultiplier,
                                    });
                                    this.setState({ sensorList: sensorList });
                                }}
                                variant="outlined"
                            >
                                <IconAdd2 /> {I18n.t("addSensor")}
                            </Button>
                        </Grid>
                    </Grid>
                </div>
            </div>
        );
    }

    renderSmokeAlarm() {
        if (this.state.addDeviceDeviceType != "smokeAlarm") return null;
        const options: { value: string; title: string }[] = [];
        this.state.addStates.map((state) => {
            //  console.log(state);
            options.push({ value: state._id, title: `${state.common.name} - ${state.common.role}` });
        });
        return (
            <div id="sensor">
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        {this.renderSelectString("selectSensor", "addDeviceSensorState", options)}
                    </Grid>
                </Grid>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        {this.renderSelect("sensorUsage", "addDeviceSensorUsage", sensorUsageOptions)}
                    </Grid>
                </Grid>
            </div>
        );
    }

    renderPresence() {
        if (this.state.addDeviceDeviceType != "presenceSensor") return null;
        const options: { value: string; title: string }[] = [];
        this.state.addStates.map((state) => {
            console.log(state);
            options.push({ value: state._id, title: `${state.common.name} - ${state.common.role}` });
        });
        return (
            <div id="sensor">
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        {this.renderSelectString("selectSensor", "addDeviceSensorState", options)}
                    </Grid>
                </Grid>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        {this.renderSelect("sensorUsage", "addDeviceSensorUsage", sensorUsageOptions)}
                    </Grid>
                </Grid>
            </div>
        );
    }

    renderToast() {
        if (!this.state.toast) {
            return null;
        }

        return (
            <Dialog
                open={true}
                onClose={() => {
                    this.setState({ toast: "" });
                }}
                fullWidth
                maxWidth="xl"
            >
                <DialogTitle>
                    {I18n.t("addNewDevice")} - {this.state.addDevice.common.name}
                </DialogTitle>
                <DialogContent>
                    <Box
                        sx={{
                            minWidth: 400,
                            width: "80%",
                        }}
                    >
                        <Grid container spacing={2}>
                            <Grid item xs={6}>
                                {this.renderInput("addDeviceName", "addDeviceName", "text")}
                            </Grid>
                            <Grid item xs={6}>
                                {this.renderInput("addDeviceConfigURL", "addDeviceConfigURL", "text")}
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                {this.renderSelect("deviceTypes", "addDeviceDeviceType", deviceTypeOptions)}
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                {this.renderBasicLamp()}
                                {this.renderDimLamp()}
                                {this.renderRGBLamp()}
                                {this.renderSensor()}
                                {this.renderSmokeAlarm()}
                                {this.renderPresence()}
                                {this.renderButton()}
                                {this.renderDoorbell()}
                                {this.renderMultiSensor()}
                                {this.renderAway()}
                            </Grid>
                        </Grid>
                    </Box>
                </DialogContent>
                <DialogActions>
                    <Button
                        onClick={() => {
                            this.setState({ toast: "" });
                        }}
                    >
                        {I18n.t("cancel")}
                    </Button>
                    <Button onClick={this.handleCreateDevice.bind(this)}>{I18n.t("addDevice")}</Button>
                </DialogActions>
            </Dialog>
        );
    }

    handleCreateDevice() {
        // const dsDevices = this.props.native.dsDevices;
        const dsDevices: Array<any> = [];
        // this.setState({ showCreateConfirmation: true });
        this.setState({ toast: "", showCreateConfirmation: true });
        // this.setConfirmationCheckbox();
        if (this.state.addDeviceDeviceType == "lamp") {
            // TODO fix objName
            dsDevices.push({
                name: this.state.addDeviceName,
                deviceType: this.state.addDeviceDeviceType,
                watchStateID: { light: this.state.basicLampPowerSwitch },
                id: this.genDSUID(),
                dsConfig: {
                    dSUID: this.genDSUID(),
                    primaryGroup: 1,
                    name: this.state.addDeviceName,
                    configURL: this.state.addDeviceConfigURL,
                    modelFeatures: {
                        blink: true,
                        dontcare: true,
                        identification: true,
                        outmode: true,
                        outvalue8: true,
                        transt: true,
                    },
                    displayId: "",
                    model: "ioBroker",
                    modelUID: this.genDSUID(),
                    modelVersion: "0.0.1",
                    vendorName: "KYUKA",
                    channelDescriptions: [
                        {
                            brightness: {
                                channelType: 1,
                                dsIndex: 0,
                                max: 100,
                                min: 0,
                                name: "brightness",
                                resolution: 0.39215686274509803,
                                siunit: "percent",
                                symbol: "%",
                            },
                        },
                    ],
                    outputDescription: [
                        {
                            name: "light",
                            dsIndex: 0,
                            maxPower: -1,
                            function: 1,
                            outputUsage: 0,
                            type: "output",
                            variableRamp: false,
                        },
                    ],
                    outputSettings: [
                        {
                            dimTimeDown: 15,
                            dimTimeDownAlt1: 162,
                            dimTimeDownAlt2: 104,
                            dimTimeUp: 15,
                            dimTimeUpAlt1: 162,
                            dimTimeUpAlt2: 104,
                            minBrightness: 0.39215686274509803,
                            onThreshold: 50,
                            pushChanges: false,
                            mode: 2,
                            groups: [0, 1],
                        },
                    ],
                },
            });
        } else if (this.state.addDeviceDeviceType == "rgbLamp") {
            // TODO fix objName
            dsDevices.push({
                name: this.state.addDeviceName,
                deviceType: this.state.addDeviceDeviceType,
                id: this.genDSUID(),
                watchStateID: {
                    switch: this.state.addDeviceRGBLampPowerSwitch,
                    switchModeColor: this.state.addDeviceRGBLampColormode,
                    brightness: this.state.addDeviceRGBLampDimmer,
                    colortemp: this.state.addDeviceRGBLampColortemp,
                    hue: this.state.addDeviceRGBLampHue,
                    saturation: this.state.addDeviceRGBLampSaturation,
                    rgb: this.state.addDeviceRGBLampRGB,
                },
                dsConfig: {
                    dSUID: this.genDSUID(),
                    primaryGroup: 1,
                    name: this.state.addDeviceName,
                    configURL: this.state.addDeviceConfigURL,
                    modelFeatures: {
                        blink: true,
                        dontcare: true,
                        identification: true,
                        outmode: true,
                        outputchannels: true,
                        outvalue8: true,
                        transt: true,
                    },
                    displayId: "",
                    model: "ioBroker",
                    modelUID: this.genDSUID(),
                    modelVersion: "0.0.1",
                    vendorName: "KYUKA",
                    channelDescriptions: [
                        {
                            brightness: {
                                channelType: 1,
                                dsIndex: 0,
                                max: 100,
                                min: 0,
                                name: "brightness",
                                resolution: 0.39215686274509803,
                                siunit: "percent",
                                symbol: "%",
                            },
                            colortemp: {
                                channelType: 4,
                                dsIndex: 3,
                                max: 1000,
                                min: 100,
                                name: "color temperature",
                                resolution: 1,
                                siunit: "mired",
                                symbol: "mired",
                            },
                            hue: {
                                channelType: 2,
                                dsIndex: 1,
                                max: 360,
                                min: 0,
                                name: "hue",
                                resolution: 0.1,
                                siunit: "degree",
                                symbol: "°",
                            },
                            saturation: {
                                channelType: 3,
                                dsIndex: 2,
                                max: 100,
                                min: 0,
                                name: "saturation",
                                resolution: 0.1,
                                siunit: "percent",
                                symbol: "%",
                            },
                            x: {
                                channelType: 5,
                                dsIndex: 4,
                                max: 1,
                                min: 0,
                                name: "CIE x",
                                resolution: 0.01,
                                siunit: "none",
                                symbol: "",
                            },
                            y: {
                                channelType: 6,
                                dsIndex: 5,
                                max: 1,
                                min: 0,
                                name: "CIE y",
                                resolution: 0.01,
                                siunit: "none",
                                symbol: "",
                            },
                        },
                    ],
                    outputDescription: [
                        {
                            name: "rgblight",
                            dsIndex: 0,
                            maxPower: -1,
                            function: 4,
                            outputUsage: 0,
                            type: "output",
                            variableRamp: true,
                        },
                    ],
                    outputSettings: [
                        {
                            dimTimeDown: 15,
                            dimTimeDownAlt1: 162,
                            dimTimeDownAlt2: 104,
                            dimTimeUp: 15,
                            dimTimeUpAlt1: 162,
                            dimTimeUpAlt2: 104,
                            minBrightness: 0.39215686274509803,
                            onThreshold: 50,
                            pushChanges: false,
                            mode: 2,
                            groups: [0, 1],
                        },
                    ],
                },
            });
        } else if (this.state.addDeviceDeviceType == "sensor") {
            dsDevices.push({
                name: this.state.addDeviceName,
                deviceType: this.state.addDeviceDeviceType,
                watchStateID: { sensor_0: this.state.addDeviceSensorState },
                modifiers: {
                    sensor_0: this.state.sensorMultiplier,
                },
                id: this.genDSUID(),
                dsConfig: {
                    dSUID: this.genDSUID(),
                    primaryGroup: this.state.addDeviceSensorColorClass,
                    name: this.state.addDeviceName,
                    configURL: this.state.addDeviceConfigURL,
                    modelFeatures: {
                        // highlevel: true,
                        // akmsensor: true,
                    },
                    displayId: "",
                    model: "ioBroker",
                    modelUID: this.genDSUID(),
                    modelVersion: "0.0.1",
                    vendorName: "KYUKA",
                    sensorDescriptions: [
                        {
                            objName: "sensor_0",
                            aliveSignInterval: 0,
                            dsIndex: 0,
                            max: this.state.addDeviceSensorMax,
                            maxPushInterval: 0,
                            min: this.state.addDeviceSensorMin,
                            name: "sensor_0",
                            resolution: this.state.addDeviceSensorResolution,
                            sensorType: this.state.addDeviceSensorType,
                            sensorUsage: this.state.addDeviceSensorUsage,
                            siunit: this.state.unitValue.unit,
                            symbol: this.state.unitValue.symbol,
                            type: "sensor",
                            updateInterval: "0",
                        },
                    ],
                },
            });
        } else if (this.state.addDeviceDeviceType == "multiSensor") {
            interface watchStateID {
                [key: string]: string;
            }
            interface sensorDescription {
                objName: string;
                aliveSignInterval: number;
                dsIndex: number;
                max: string;
                maxPushInterval: number;
                min: string;
                name: string;
                resolution: string;
                sensorType: string;
                sensorUsage: string;
                siunit: string;
                symbol: string;
                type: string;
                updateInterval: string;
            }
            interface sensorSetting {
                group: string;
                minPushInterval: number;
                changesOnlyInterval: number;
                objName: string;
            }
            interface sensorModifier {
                [key: string]: string;
            }
            const watchStateID: watchStateID = {};
            const sensorDescription: Array<sensorDescription> = [];
            const sensorSetting: Array<sensorSetting> = [];
            const sensorModifiers: sensorModifier = {};
            this.state.sensorList.forEach((s, i) => {
                watchStateID[`sensor_${i}`] = s.selectSensor;
                sensorModifiers[`sensor_${i}`] = s.modifier;
                sensorDescription.push({
                    objName: `sensor_${i}`,
                    aliveSignInterval: 0,
                    dsIndex: i,
                    max: s.addDeviceSensorMax,
                    maxPushInterval: 0,
                    min: s.addDeviceSensorMin,
                    name: `sensor_${i}`,
                    resolution: s.addDeviceSensorResolution,
                    sensorType: s.sensorType,
                    sensorUsage: s.sensorUsage,
                    siunit: s.addDeviceSensorSIUnit,
                    symbol: s.addDeviceSensorSymbol,
                    type: "sensor",
                    updateInterval: "10",
                });
                sensorSetting.push({
                    group: s.colorClass,
                    minPushInterval: 2,
                    changesOnlyInterval: 0,
                    objName: `sensor_${i}`,
                });
            });
            dsDevices.push({
                name: this.state.addDeviceName,
                deviceType: this.state.addDeviceDeviceType,
                watchStateID: watchStateID,
                modifiers: sensorModifiers,
                id: this.genDSUID(),
                dsConfig: {
                    dSUID: this.genDSUID(),
                    primaryGroup: this.state.addDeviceSensorColorClass,
                    name: this.state.addDeviceName,
                    configURL: this.state.addDeviceConfigURL,
                    modelFeatures: {
                        // highlevel: true,
                        // akmsensor: true,
                    },
                    displayId: "",
                    model: "ioBroker",
                    modelUID: this.genDSUID(),
                    modelVersion: "0.0.1",
                    vendorName: "KYUKA",
                    sensorDescriptions: sensorDescription,
                    sensorSettings: sensorSetting,
                },
            });
        } else if (this.state.addDeviceDeviceType == "smokeAlarm") {
            dsDevices.push({
                name: this.state.addDeviceName,
                deviceType: this.state.addDeviceDeviceType,
                watchStateID: { generic_0: this.state.addDeviceSensorState },
                id: this.genDSUID(),
                dsConfig: {
                    dSUID: this.genDSUID(),
                    primaryGroup: 8,
                    name: this.state.addDeviceName,
                    configURL: this.state.addDeviceConfigURL,
                    modelFeatures: {
                        highlevel: true,
                        jokerconfig: true,
                        akmsensor: true,
                    },
                    displayId: "",
                    model: "ioBroker",
                    modelUID: this.genDSUID(),
                    modelVersion: "0.0.1",
                    vendorName: "KYUKA",
                    vendorId: "vendorname:kyuka.ch",
                    binaryInputDescriptions: [
                        {
                            name: "generic_0",
                            objName: "generic_0",
                            dsIndex: 0,
                            inputType: 0,
                            inputUsage: 0,
                            sensorFunction: 7,
                            updateInterval: 0,
                            type: "binaryInput",
                        },
                    ],
                    binaryInputSettings: [
                        {
                            // changesOnlyInterval: 1800,
                            group: 8,
                            // minPushInterval: 2,
                            sensorFunction: 7,
                            inputName: "generic_0",
                            objName: "generic_0",
                        },
                    ],
                },
            });
        } else if (this.state.addDeviceDeviceType == "presenceSensor") {
            dsDevices.push({
                name: this.state.addDeviceName,
                deviceType: this.state.addDeviceDeviceType,
                watchStateID: { generic_0: this.state.addDeviceSensorState },
                id: this.genDSUID(),
                dsConfig: {
                    dSUID: this.genDSUID(),
                    primaryGroup: 8,
                    name: this.state.addDeviceName,
                    configURL: this.state.addDeviceConfigURL,
                    modelFeatures: {
                        highlevel: true,
                        akmsensor: true,
                    },
                    displayId: "",
                    model: "ioBroker",
                    modelUID: this.genDSUID(),
                    modelVersion: "0.0.1",
                    vendorName: "KYUKA",
                    binaryInputDescriptions: [
                        {
                            // aliveSignInterval: 360,
                            name: "generic_0",
                            objName: "generic_0",
                            dsIndex: 0,
                            inputType: 0,
                            inputUsage: 0,
                            // maxPushInterval: 3300,
                            updateInterval: 0,
                            sensorFunction: 5,
                            // type: "binaryInput",
                        },
                    ],
                    binaryInputSettings: [
                        {
                            // changesOnlyInterval: 1800,
                            group: 8,
                            inputName: "generic_0",
                            objName: "generic_0",
                            // minPushInterval: 2,
                            sensorFunction: 5,
                        },
                    ],
                },
            });
        } else if (this.state.addDeviceDeviceType == "button") {
            dsDevices.push({
                name: this.state.addDeviceName,
                deviceType: this.state.addDeviceDeviceType,
                watchStateID: { button_0: this.state.basicButton },
                id: this.genDSUID(),
                dsConfig: {
                    dSUID: this.genDSUID(),
                    primaryGroup: 8,
                    name: this.state.addDeviceName,
                    configURL: this.state.addDeviceConfigURL,
                    modelFeatures: {
                        highlevel: true,
                        jokerconfig: true,
                        pushbadvanced: true,
                        pushbarea: true,
                        pushbutton: true,
                    },
                    displayId: "",
                    model: "ioBroker",
                    modelUID: this.genDSUID(),
                    modelVersion: "0.0.1",
                    vendorName: "KYUKA",
                    vendorId: "vendorname:kyuka.ch",
                    buttonInputDescriptions: [
                        {
                            buttonElementID: 0,
                            buttonID: 0,
                            buttonType: 1,
                            combinables: 0,
                            dsIndex: 0,
                            name: "button_id0_el0",
                            supportsLocalKeyMode: 0,
                            type: "buttonInput",
                            objName: "button_0",
                            // "x-p44-behaviourType": "buttonInput",
                        },
                    ],
                    buttonInputSettings: [
                        {
                            callsPresent: 0,
                            channel: 0,
                            function: 0,
                            group: 1,
                            mode: 0,
                            setsLocalPriority: 0,
                            objName: "button_0",
                            // "x-p44-buttonActionId": 0,
                            // "x-p44-buttonActionMode": 255,
                            // "x-p44-longFunctionDelay": "",
                            // "x-p44-stateMachineMode": 0,
                        },
                    ],
                },
            });
        } else if (this.state.addDeviceDeviceType == "awayButton") {
            dsDevices.push({
                name: this.state.addDeviceName,
                deviceType: this.state.addDeviceDeviceType,
                watchStateID: { button_0: this.state.basicButton },
                id: this.genDSUID(),
                dsConfig: {
                    dSUID: this.genDSUID(),
                    primaryGroup: 8,
                    name: this.state.addDeviceName,
                    configURL: this.state.addDeviceConfigURL,
                    modelFeatures: {
                        highlevel: true,
                        jokerconfig: true,
                        pushbadvanced: true,
                        pushbarea: true,
                        pushbutton: true,
                    },
                    displayId: "",
                    model: "ioBroker",
                    modelUID: this.genDSUID(),
                    modelVersion: "0.0.1",
                    vendorName: "KYUKA",
                    vendorId: "vendorname:kyuka.ch",
                    buttonInputDescriptions: [
                        {
                            buttonElementID: 0,
                            buttonID: 0,
                            buttonType: 1,
                            combinables: 0,
                            dsIndex: 0,
                            name: "button_id0_el0",
                            supportsLocalKeyMode: 0,
                            type: "buttonInput",
                            objName: "button_0",
                            // "x-p44-behaviourType": "buttonInput",
                        },
                    ],
                    buttonInputSettings: [
                        {
                            callsPresent: 0,
                            channel: 0,
                            function: 3,
                            group: 0,
                            mode: 0,
                            setsLocalPriority: 0,
                            objName: "button_0",
                            // "x-p44-buttonActionId": 0,
                            // "x-p44-buttonActionMode": 255,
                            // "x-p44-longFunctionDelay": "",
                            // "x-p44-stateMachineMode": 0,
                        },
                    ],
                },
            });
        } else if (this.state.addDeviceDeviceType == "doorbell") {
            dsDevices.push({
                name: this.state.addDeviceName,
                deviceType: this.state.addDeviceDeviceType,
                watchStateID: { button_0: this.state.basicDoorbell },
                id: this.genDSUID(),
                dsConfig: {
                    dSUID: this.genDSUID(),
                    primaryGroup: 8,
                    name: this.state.addDeviceName,
                    configURL: this.state.addDeviceConfigURL,
                    modelFeatures: {
                        highlevel: true,
                        jokerconfig: true,
                        pushbadvanced: true,
                        pushbarea: true,
                        pushbutton: true,
                    },
                    displayId: "",
                    model: "ioBroker",
                    modelUID: this.genDSUID(),
                    modelVersion: "0.0.1",
                    vendorName: "KYUKA",
                    vendorId: "vendorname:kyuka.ch",
                    buttonInputDescriptions: [
                        {
                            buttonElementID: 0,
                            buttonID: 0,
                            buttonType: 1,
                            /// combinables: 0,
                            dsIndex: 0,
                            name: "button_id0_el0",
                            supportsLocalKeyMode: 0,
                            type: "buttonInput",
                            objName: "button_0",
                            // "x-p44-behaviourType": "buttonInput",
                        },
                    ],
                    buttonInputSettings: [
                        {
                            callsPresent: 0,
                            channel: 0,
                            function: 5,
                            group: 8,
                            mode: 0,
                            setsLocalPriority: 0,
                            objName: "button_0",
                            // "x-p44-buttonActionId": 0,
                            // "x-p44-buttonActionMode": 255,
                            // "x-p44-longFunctionDelay": "",
                            // "x-p44-stateMachineMode": 0,
                        },
                    ],
                },
            });
        }
        // this.props.onChange("dsDevice", dsDevices);
        this.props.sendToAddDevice(dsDevices[0]).then(() => {
            setTimeout(() => {
                this.props.updateLastDeviceUpdate();
            }, 3 * 1000);
        });

        this.setState({ sensorList: [] });

        // console.log("confirmMsg", this.state.showCreateConfirmation);

        // switch tab back to device settings
        // this.props.onChange("tabValue", 0);
    }

    resetSnackbar() {
        this.setState({ showCreateConfirmation: false });
    }

    renderDeviceCreate() {
        // if (!this.state.confirmMsg) return null;

        return (
            <Snackbar
                key={Math.random()}
                anchorOrigin={{
                    vertical: "top",
                    horizontal: "right",
                }}
                autoHideDuration={6000}
                open={this.state.showCreateConfirmation}
                onClose={this.resetSnackbar.bind(this)}
            >
                <Alert severity="success" sx={{ width: "100%" }}>
                    {I18n.t("successfulDeviceCreate")}
                </Alert>
            </Snackbar>
        );
    }

    addNewDeviceManual(event: any, device: any) {
        // this.setState({ channelFilter: "" });
        // console.log("ADD NEW DEVICE", device);

        const deviceStates = [{ _id: device._id, common: { name: device._id, role: "" } }];
        // console.log(deviceStates);
        this.setState({
            addDevice: device,
            addStates: deviceStates,
            toast: "New Device",
        });
    }

    renderManualAdd() {
        return (
            <TextField
                label={I18n.t("manualDeviceId")}
                className={`${this.props.classes.input} ${this.props.classes.controlElement}`}
                value={this.state.manualDeviceId}
                onChange={(e) => this.setState({ manualDeviceId: e.target.value })}
                type="text"
                margin="normal"
            />
        );
    }

    /* renderSelectIdDialog() {
        if (this.state.showSelectId) {
            return (
                <DialogSelectID
                    key="tableSelect"
                    imagePrefix="../.."
                    dialogName={this.props.adapterName}
                    themeType="light"
                    socket={this.props.socket}
                    statesOnly={true}
                    selected={this.state.selectIdValue}
                    onClose={() => this.setState({ showSelectId: false })}
                    onOk={(selected) => {
                        this.setState({ showSelectId: false, selectIdValue: selected });
                    }}
                />
            );
        } else {
            return null;
        }
    }*/

    checkConfirmationCheckbox(): boolean {
        console.log(this.state.showCreateConfirmation);
        return this.state.showCreateConfirmation;
    }

    setConfirmationCheckbox(): void {
        console.log("set state");
        this.setState({
            showCreateConfirmation: true,
        });
        console.log(this.state.showCreateConfirmation);
    }

    render() {
        // console.log(table);

        return (
            <div id="addDevices">
                <br />
                {this.renderToast()}
                {this.renderDeviceCreate()}
                <form className={this.props.classes.tab}>
                    <h3>{I18n.t("searchNewDevices")}</h3>
                    <Grid container spacing={2}>
                        <Grid item xs={6}>
                            {this.renderManualAdd()}
                        </Grid>
                        <Grid item xs={6}>
                            <Button
                                onClick={(e) => {
                                    this.addNewDeviceManual(e, {
                                        _id: this.state.manualDeviceId,
                                        common: { name: this.state.manualDeviceId },
                                    });
                                }}
                                variant="outlined"
                            >
                                <IconAdd /> {I18n.t("addButton")}
                            </Button>
                        </Grid>
                    </Grid>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            {this.renderChannelFilter()}
                        </Grid>
                    </Grid>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            {this.renderAvailableChannels()}
                        </Grid>
                    </Grid>
                </form>
            </div>
        );
    }
}

export default withStyles(styles)(AddDevices);
