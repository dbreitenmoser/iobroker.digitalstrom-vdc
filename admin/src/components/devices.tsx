import React, { useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { CreateCSSProperties } from "@material-ui/core/styles/withStyles";
import MaUTable from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

import Button from "@material-ui/core/Button";
import IconRemove from "@material-ui/icons/Remove";

import Alert from "@material-ui/lab/Alert";
import Connection from "@iobroker/adapter-react/Connection";

// import { Typography } from "@material-ui/core";
import I18n from "@iobroker/adapter-react/i18n";

const styles = (): Record<string, CreateCSSProperties> => ({
    input: {
        marginTop: 0,
        minWidth: 400,
    },
    button: {
        marginRight: 20,
    },
    card: {
        maxWidth: 345,
        textAlign: "center",
    },
    media: {
        height: 180,
    },
    column: {
        display: "inline-block",
        verticalAlign: "top",
        marginRight: 20,
    },
    columnLogo: {
        width: 350,
        marginRight: 0,
    },
    columnSettings: {
        width: "calc(100% - 370px)",
    },
    controlElement: {
        //background: "#d2d2d2",
        marginBottom: 5,
    },
});

interface DevicesProps {
    classes: Record<string, string>;
    native: Record<string, any>;
    socket: Connection;
    adapterName: string;
    sendToVanish: any;
    sendToRemoveDevice: any;
    sendToRemoveDevices: any;
    sendToListDevices: any;
    allObjects: Array<any>;
    allDevices: any;
    lastDeviceUpdate: string | undefined | null;
    updateLastDeviceUpdate: any;
    onChange: (attr: string, value: any) => void;
}

interface DevicesState {
    // add your state properties here
    dummy?: undefined;
    allOwnDevices: any;
}

class Devices extends React.Component<DevicesProps, DevicesState> {
    static displayName = "Devices";

    constructor(props: DevicesProps) {
        super(props);
        this.state = {
            allOwnDevices: [],
        };
    }

    componentDidMount() {
        console.log("MOUNTED");

        this.listDevices(true);
        /* this.props.socket.getObjects(true, true).then((ob) => {
            console.log(ob);
            this.setState({
                allObjects: ob,
            });
        });*/
    }

    removeDevice(row) {
        // if (row.watchStateID && row.watchStateID.length > 0) {
        /* this.props.sendToVanish(row.dsConfig.dSUID);
            this.props.onChange(
                "dsDevices",
                this.props.native.dsDevices.filter((d) => d.watchStateID != row.watchStateID),
            ); */
        console.log(row);

        this.props.sendToRemoveDevice(row.id, row.dsConfig.dSUID).then(() => {
            console.log("Refreshing device list");
            this.listDevices(false);
        });

        // }
    }

    listDevices(delay = false) {
        const sec = delay ? 2 : 0;
        setTimeout(() => {
            this.props.sendToListDevices().then((response) => {
                console.log(JSON.stringify(response));
                /* if (typeof response.devices == "object") {
                    this.setState({ allOwnDevices: [response.devices] });
                } else {
                    this.setState({ allOwnDevices: response.devices });
                } */
                this.setState({ allOwnDevices: response });
            });
        }, sec * 1000);
    }

    renderEmptyDevices() {
        if (this.props.native.dsDevices.length > 0) return null;
        return (
            <div className="alert-container">
                <Alert color="info">{I18n.t("noDevicesConfigured")}</Alert>
            </div>
        );
    }

    renderDevices() {
        // Render the UI for your table
        // const devices = this.props.sendToListDevices() || [];
        console.log("DEVS: " + JSON.stringify(this.state.allOwnDevices));
        if (
            !this.state.allOwnDevices ||
            this.state.allOwnDevices.length == 0 ||
            Object.keys(this.state.allOwnDevices[0]).length === 0
        )
            return null;
        // const devs: Array<any> = this.state.allOwnDevices;
        /*this.state.allOwnDevices.forEach((d) => {
            const currentDevice = this.props.allObjects.find((a) => a._id == Object.keys(d)[0]);
            const watchStates: Array<any> = [];
            if (typeof currentDevice.native.deviceObj.watchStateID == "object") {
                for (const [key, value] of Object.entries(currentDevice.native.deviceObj.watchStateID)) {
                    watchStates.push(`${key} : ${value}`);
                }
            } else {
                watchStates.push(currentDevice.native.deviceObj.watchStateID);
            }
            devs.push({
                name: currentDevice.common.name,
                id: Object.keys(d)[0],
                watchStates: watchStates,
            });
            console.log(this.props.allObjects);
            console.log(d);
        });*/
        return (
            <div className="container">
                <MaUTable>
                    <TableHead>
                        <TableRow>
                            <TableCell key="1">{I18n.t("devicesName")}</TableCell>
                            <TableCell key="2">{I18n.t("devicesWatchState")}</TableCell>
                            <TableCell key="3">{I18n.t("devicesAction")}</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.state.allOwnDevices.map((d, i) => (
                            <React.Fragment key={i}>
                                <TableRow>
                                    <TableCell>{d.name}</TableCell>
                                    {this.renderWatchStateID(d.watchStateID)}
                                    <TableCell>
                                        <Button
                                            onClick={() => {
                                                this.removeDevice(d);
                                            }}
                                            variant="outlined"
                                        >
                                            <IconRemove /> {I18n.t("delButton")}
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            </React.Fragment>
                        ))}
                    </TableBody>
                </MaUTable>
            </div>
        );
    }

    renderWatchStateID(states) {
        if (typeof states == "object") {
            return (
                <TableCell>
                    {Object.entries(states).map(([key, value]) => (
                        <li key={key}>{value as string}</li>
                    ))}
                </TableCell>
            );
        } else {
            return <TableCell>{states}</TableCell>;
        }
    }

    removeDevices() {
        this.props.native.dsDevices.forEach((d) => {
            console.log(d);
            console.log(this.props.adapterName);
            if (d.dsConfig && d.dsConfig.dSUID) {
                /*this.props.socket.sendTo(this.props.adapterName, "sendVanishDevice", "blah").then((response) => {
                    console.log(response);
                });*/
                {
                    this.props.sendToVanish(d.dsConfig.dSUID);
                }
            }
        });
        this.props.onChange("dsDevices", []);
    }

    renderDeleteAll() {
        if (this.props.native.dsDevices.length == 0) return null;
        return (
            <Button
                onClick={() => {
                    this.removeDevices();
                }}
                variant="outlined"
            >
                <IconRemove /> {I18n.t("deleteAllDevices")}
            </Button>
        );
    }

    render() {
        // console.log(table);

        return (
            <div id="Devices">
                <br />
                <form className={this.props.classes.tab}>
                    <h3>{I18n.t("existingDevices")}</h3>
                    {this.renderEmptyDevices()}
                    {this.renderDevices()}
                    {this.renderDeleteAll()}
                </form>
            </div>
        );
    }
}

export default withStyles(styles)(Devices);
