import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { CreateCSSProperties } from "@material-ui/core/styles/withStyles";
import TextField from "@material-ui/core/TextField";
import Input from "@material-ui/core/Input";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
// import { Typography } from "@material-ui/core";
import I18n from "@iobroker/adapter-react/i18n";
import Connection from "@iobroker/adapter-react/Connection";

import { Tabs, Tab } from "@material-ui/core";
import PropTypes from "prop-types";

import AddDevices from "./addDevices";
import Devices from "./devices";
import { Alert } from "@material-ui/lab";

const styles = (): Record<string, CreateCSSProperties> => ({
    input: {
        marginTop: 0,
        minWidth: 400,
    },
    button: {
        marginRight: 20,
    },
    card: {
        maxWidth: 345,
        textAlign: "center",
    },
    media: {
        height: 180,
    },
    column: {
        display: "inline-block",
        verticalAlign: "top",
        marginRight: 20,
    },
    columnLogo: {
        width: 350,
        marginRight: 0,
    },
    columnSettings: {
        width: "calc(100% - 370px)",
    },
    controlElement: {
        //background: "#d2d2d2",
        marginBottom: 5,
    },
});

interface SettingsProps {
    classes: Record<string, string>;
    native: Record<string, any>;
    allObjects: Array<any>;
    allDevices: any
    socket: Connection;
    adapterName: string;
    instance: number;
    sendToVanish: any;
    sendToListDevices: any;
    sendToAddDevice: any;
    sendToRemoveDevice: any;
    sendToRemoveDevices: any;
    sendToCheckRunning: any;
    onChange: (attr: string, value: any) => void;
}

interface SettingsState {
    // add your state properties here
    dummy?: undefined;
    channelFilter: string;
    toast: string;
    addDevice: any;
    addStates: Array<any>;
    tabValue: number;
    VDCRunningState: boolean | undefined;
    lastDeviceUpdate: string | undefined | null;

}

class Settings extends React.Component<SettingsProps, SettingsState> {
    static displayName = "Settings";

    constructor(props: SettingsProps) {
        super(props);
        this.state = {
            channelFilter: "",
            toast: "",
            addDevice: {},
            addStates: [],
            tabValue: 0,
            VDCRunningState: false,
            lastDeviceUpdate: null
        };
    }

    componentDidMount() {
        console.log("MOUNTED");
        this.props.socket.subscribeState(`DS-Devices.VDC.running`, false, this.onObjectChange.bind(this) ));
        this.checkVDCRunningState();
        /* this.props.socket.getObjects(true, true).then((ob) => {
            console.log(ob);
            this.setState({
                allObjects: ob,
            });
        });*/
    }

    onObjectChange(id, obj, oldObj):void {
        console.log('onObjectChange',id,obj,oldObj)
        this.setState({VDCRunningState: obj.val})
        console.log(this.state.VDCRunningState)
    }

    renderInput(title: AdminWord, attr: string, type: string) {
        return (
            <TextField
                label={I18n.t(title)}
                className={`${this.props.classes.input} ${this.props.classes.controlElement}`}
                value={this.props.native[attr]}
                type={type || "text"}
                onChange={(e) => this.props.onChange(attr, e.target.value)}
                margin="normal"
            />
        );
    }

    renderSelect(
        title: AdminWord,
        attr: string,
        options: { value: string; title: AdminWord }[],
        style?: React.CSSProperties,
    ) {
        return (
            <FormControl
                className={`${this.props.classes.input} ${this.props.classes.controlElement}`}
                style={{
                    paddingTop: 5,
                    ...style,
                }}
            >
                <Select
                    value={this.props.native[attr] || "_"}
                    onChange={(e) => this.props.onChange(attr, e.target.value === "_" ? "" : e.target.value)}
                    input={<Input name={attr} id={attr + "-helper"} />}
                >
                    {options.map((item) => (
                        <MenuItem key={"key-" + item.value} value={item.value || "_"}>
                            {I18n.t(item.title)}
                        </MenuItem>
                    ))}
                </Select>
                <FormHelperText>{I18n.t(title)}</FormHelperText>
            </FormControl>
        );
    }

    renderCheckbox(title: AdminWord, attr: string, style?: React.CSSProperties) {
        return (
            <FormControlLabel
                key={attr}
                style={{
                    paddingTop: 5,
                    ...style,
                }}
                className={this.props.classes.controlElement}
                control={
                    <Checkbox
                        checked={this.props.native[attr]}
                        onChange={() => this.props.onChange(attr, !this.props.native[attr])}
                        color="primary"
                    />
                }
                label={I18n.t(title)}
            />
        );
    }

    renderChannelFilter() {
        return (
            <TextField
                label={I18n.t("channelFilter")}
                className={`${this.props.classes.input} ${this.props.classes.controlElement}`}
                value={this.state.channelFilter}
                type="text"
                onChange={(e) => this.setState({ channelFilter: e.target.value })}
                margin="normal"
            />
        );
    }

    genDSUID() {
        const genRanHex = (size) => [...Array(size)].map(() => Math.floor(Math.random() * 16).toString(16)).join("");
        // const instanceName = `${this.props.adapterName}.${this.props.instance}`;
        /*this.props.socket.sendTo(instanceName, "genSDUID", "blah").then((response) => {
            console.log(response);
        });*/
        this.props.onChange("vdcDSUID", genRanHex(34).toUpperCase());
    }

    updateLastDeviceUpdate() {
        this.setState({'lastDeviceUpdate': new Date().toString})
    }

    generateVDCDSUID() {
        return (
            <Button
                onClick={() => {
                    this.genDSUID();
                }}
                variant="outlined"
            >
                {I18n.t("generateVDCDSUID")}
            </Button>
        );
    }

    a11yProps(index) {
        return {
            id: `simple-tab-${index}`,
            "aria-controls": `simple-tabpanel-${index}`,
        };
    }

    checkVDCRunningState() {
         setTimeout(() => {
            this.props.sendToCheckRunning().then((response) => {
                console.log(response);
                this.setState({ VDCRunningState: response.state.val });
            });
            
        }, 3 * 1000);
    }

    renderNotRunning() {
        if (this.state.VDCRunningState) return null;
        return (
            <div className="alert-container">
                <Alert color="warning">{I18n.t("vdcNotRunning")}</Alert>
            </div>
        );
    }

    renderTabs() {
        function TabPanel(props) {
            const { children, value, index, ...other } = props;

            return (
                <div
                    role="tabpanel"
                    hidden={value !== index}
                    id={`simple-tabpanel-${index}`}
                    aria-labelledby={`simple-tab-${index}`}
                    {...other}
                >
                    {value === index && <Box sx={{ p: 3 }}>{children}</Box>}
                </div>
            );
        }

        TabPanel.propTypes = {
            children: PropTypes.node,
            index: PropTypes.number.isRequired,
            value: PropTypes.number.isRequired,
        };

        const handleChange = (event, newValue) => {
            this.setState({ tabValue: newValue });
        };

        // const table = this.renderAvailableChannels();

        return (
            <div className="tabs">
                <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
                    <Tabs value={this.state.tabValue} onChange={handleChange} aria-label="basic tabs example">
                        <Tab label={I18n.t("tabDevices")} {...this.a11yProps(0)} />
                        <Tab label={I18n.t("tabaddDevices")} {...this.a11yProps(0)} />
                        <Tab label={I18n.t("tabGeneralSettings")} {...this.a11yProps(1)} />
                    </Tabs>
                </Box>
                <TabPanel value={this.state.tabValue} index={0}>
                    <Devices
                        native={this.props.native}
                        adapterName={this.props.adapterName}
                        socket={this.props.socket}
                        onChange={this.props.onChange}
                        sendToVanish={this.props.sendToVanish}
                        sendToRemoveDevice={this.props.sendToRemoveDevice}
                        sendToRemoveDevices={this.props.sendToRemoveDevices}
                        sendToListDevices={this.props.sendToListDevices}
                        allDevices={this.props.allDevices}
                        allObjects={this.props.allObjects}
                        lastDeviceUpdate={this.state.lastDeviceUpdate}
                        updateLastDeviceUpdate={this.updateLastDeviceUpdate.bind(this)}
                    />
                </TabPanel>
                <TabPanel value={this.state.tabValue} index={1}>
                    <AddDevices
                        native={this.props.native}
                        allObjects={this.props.allObjects}
                        socket={this.props.socket}
                        adapterName={this.props.adapterName}
                        instance={this.props.instance}
                        tabValue={this.state.tabValue}
                        sendToAddDevice={this.props.sendToAddDevice}
                        onChange={this.props.onChange}
                        updateLastDeviceUpdate={this.updateLastDeviceUpdate.bind(this)}
                    />
                </TabPanel>
                <TabPanel value={this.state.tabValue} index={2}>
                    <br />
                    {this.renderNotRunning()}
                    <form className={this.props.classes.tab}>
                        <h3>{I18n.t("defaultSettings")}</h3>
                        <Grid container spacing={2}>
                            <Grid item xs={6}>
                                {this.renderInput("vdcName", "vdcName", "text")}
                            </Grid>
                            <Grid item xs={6}>
                                <Grid item xs={6}>
                                    {this.renderInput("vdcDSUID", "vdcDSUID", "text")}
                                </Grid>
                                <Grid item xs={6}>
                                    {this.generateVDCDSUID()}
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={6}>
                                {this.renderInput("vdcPort", "vdcPort", "text")}
                            </Grid>
                            <Grid item xs={6}>
                                {this.renderCheckbox("vdcDebug", "vdcDebug")}
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={6}>
                                {this.renderInput("vdcConfigURL", "vdcConfigURL", "text")}
                            </Grid>
                        </Grid>
                    </form>
                </TabPanel>
            </div>
        );
    }

    render() {
        // console.log(table);

        return <Box>{this.renderTabs()}</Box>;
    }
}

export default withStyles(styles)(Settings);
