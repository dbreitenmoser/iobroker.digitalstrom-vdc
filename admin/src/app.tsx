import React from "react";
import { Theme, withStyles } from "@material-ui/core/styles";

import GenericApp from "@iobroker/adapter-react/GenericApp";
import Settings from "./components/settings";
import { GenericAppProps, GenericAppSettings } from "@iobroker/adapter-react/types";
import { StyleRules } from "@material-ui/core/styles";

// import Utils from "@iobroker/adapter-react/Components/Utils";

const styles = (_theme: Theme): StyleRules => ({
    root: {},
});

class App extends GenericApp {
    constructor(props: GenericAppProps) {
        const extendedProps: GenericAppSettings = {
            ...props,
            encryptedFields: [],
            translations: {
                en: require("./i18n/en.json"),
                de: require("./i18n/de.json"),
            },
        };
        super(props, extendedProps);
    }

    onConnectionReady(): void {
        // executed when connection is ready
        this.socket.getObjects(true, true).then((allObjects) => {
            // console.log(allObjects);
            const arrayOfObj: Array<any> = Object.entries(allObjects).map((e) => e[1]);
            this.setState({ allObjects: arrayOfObj });
        });

        // this.sendToListDevices();

        // this.socket.subscribeState(`DS-Devices.VDC.running`, false, this.onObjectChange.bind(this) ));

        // const instanceName = this.adapterName + "." + this.instance;
        /* this.socket.sendTo(this.instanceId, "send", "blah").then(function (response) {
            console.log(response);
        });*/
    }

    /* onObjectChange(id, obj, oldObj):void {
        console.log('onObjectChange',id,obj,oldObj)
        this.setState({VDCRunningState: obj.val})
        console.log(this.state.VDCRunningState)
    } */

    sendToVanish(dSUID: string): void {
        console.log("dSUID: ", dSUID);
        this.socket.sendTo(this.adapterName, "sendVanishDevice", dSUID);
    }

    sendToAddDevice(obj: any): Promise<any> {
        console.log("obj: ", JSON.stringify(obj));
        // const self = this;
        return this.socket.sendTo(this.adapterName, "sendAddDevice", obj);
    }

    sendToListDevices(): Promise<any> {
        console.log("get devicelist");
        return this.socket.sendTo(this.adapterName, "sendListDevices");
    }

    sendToRemoveDevice(id: string, dSUID: string): Promise<any> {
        console.log("dSUID: ", { id: id, dSUID: dSUID });
        return this.socket.sendTo(this.adapterName, "sendRemoveDevice", { id: id, dSUID: dSUID });
    }

    sendToRemoveDevices(): Promise<any> {
        return this.socket.sendTo(this.adapterName, "sendRemoveDevices");
    }

    sendToCheckRunning(): Promise<any> {
        return this.socket.sendTo(this.adapterName, "getRunning");
    }

    render() {
        if (!this.state.loaded) {
            return super.render();
        }
        /*  */

        return (
            <div className="App">
                {this.renderError()}
                {this.renderToast()}
                <Settings
                    native={this.state.native}
                    allObjects={this.state.allObjects}
                    socket={this.socket}
                    adapterName={this.instanceId}
                    instance={this.instance}
                    sendToCheckRunning={this.sendToCheckRunning}
                    sendToVanish={this.sendToVanish}
                    sendToRemoveDevice={this.sendToRemoveDevice}
                    sendToRemoveDevices={this.sendToRemoveDevices}
                    sendToAddDevice={this.sendToAddDevice}
                    sendToListDevices={this.sendToListDevices}
                    VDCRunningState={this.state.VDCRunningState}
                    allDevices={this.state.allDevices}
                    onChange={(attr, value) => this.updateNativeValue(attr, value)}
                />
                {this.renderSaveCloseButtons()}
            </div>
        );
    }
}

export default withStyles(styles)(App);
